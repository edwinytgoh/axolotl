## vNEXT

### Enhancements

Upgrade dependencies:
[#4](https://gitlab.com/axolotl1/axolotl/-/issues/4)
[!24](https://gitlab.com/axolotl1/axolotl/-/merge_requests/24)

### Bugfixes

### Other

Remove Autokeras Wrapper:
[#5](https://gitlab.com/axolotl1/axolotl/-/issues/5)
[!25](https://gitlab.com/axolotl1/axolotl/-/merge_requests/25)
